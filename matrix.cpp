#include "matrix.h"

matrix::matrix(int rows, int cols)
{
    init(rows, cols);
}

matrix::matrix(int rows)
{
    init(rows, rows);
}

matrix::matrix()
{
    this->rows = 0;
    this->cols = 0;
}

matrix& matrix::init(int rows, int cols)
{
    if(rows > 0 && cols > 0)
    {
        this->rows = rows;
        this->cols = cols;

        this->mat = new int*[rows];

        for(unsigned int i = 0; i < rows; ++i)
            mat[i] = new int[cols];
    }

    return *this;
}

matrix& matrix::init(int rows)
{
    init(rows, rows);
    return *this;
}

std::istream& operator >> (std::istream& stream, matrix& obj)
{
    if(obj.rows == 0 && obj.cols == 0)
        return stream;
    
    for(unsigned int i = 0; i < obj.rows; ++i)
        for(unsigned int j = 0; j < obj.rows; ++j)
            stream >> obj.mat[i][j]; 
    
    return stream;
}

std::ostream& operator << (std::ostream& stream, const matrix& obj)
{
    if(obj.rows == 0 && obj.cols == 0)
        return stream;
    
    for(unsigned int i = 0; i < obj.rows; ++i)
    {
        for(unsigned int j = 0; j < obj.rows; ++j)
        {
            stream << obj.mat[i][j] << '\t'; 
        }
        
        stream << std::endl;
    }

    return stream;
}
