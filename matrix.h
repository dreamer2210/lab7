#pragma once

#include <istream>
#include <ostream>

class matrix
{
private:
    int** mat;
    unsigned int rows;
    unsigned int cols;
public:
    matrix(int rows, int cols);
    matrix(int rows);
    matrix();

    matrix& init(int rows, int cols);
    matrix& init(int rows);

    int* operator [] (int i)
    {
        return mat[i];
    }

    const int* operator [] (int i) const
    {
        return mat[i];
    }

    friend std::istream& operator >> (std::istream& stream, matrix& obj);
    friend std::ostream& operator << (std::ostream& stream, const matrix& obj);
};
