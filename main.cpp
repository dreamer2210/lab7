#include <iostream>
#include <fstream>
#include <math.h>
#include "matrix.h"

#define FILE_IN "input.txt"
#define FILE_OUT "output.txt"

using namespace std;

int main(int argc, char* argv[])
{
    fstream fin;
    fstream fout;
    matrix mat;           // ������� ������

    unsigned int size = 0;
    int temp, iMin, min;

    fin.open(FILE_IN);

    if(!fin)
        return -1;
    
    while(!fin.eof())
    {
        fin >> temp;
        ++size;
    }

    size = sqrt(size);

    fin.close();

    fin.open(FILE_IN);
    if(!fin)
        return -1;

    mat.init(size);
    fin >> mat;

    fin.close();

    unsigned int minDist[size];              // ����������� ����������
    int points[size];                       // ���������� �������

    cout << mat << endl;

    //������������� ������ � ����������
    for (unsigned int i = 0; i < size; i++)
    {
        minDist[i] = 10000;
        points[i] = 1;
    }

    minDist[0] = 0;

    // ��� ���������
    do 
    {
        iMin = 10000;
        min = 10000;

        for (unsigned int i = 0; i < size; i++)
        { 
            // ���� ������� ��� �� ������ � ��� ������ min
            if ((points[i] == 1) && (minDist[i] < min))
            { 
                // ��������������� ��������
                min = minDist[i];
                iMin = i;
            }
        }
        // ��������� ��������� ����������� ���
        // � �������� ���� �������
        // � ���������� � ������� ����������� ����� �������
        if (iMin != 10000)
        {
            for (unsigned int i = 0; i < size; i++)
            {
                if (mat[iMin][i] > 0)
                {
                    temp = min + mat[iMin][i];

                    if (temp < minDist[i])
                    {
                        minDist[i] = temp;
                    }
                }
            }
            points[iMin] = 0;
        }
    } while (iMin < 10000);
    
    // ����� ���������� ���������� �� ������
    cout << "\n���������� ���������� �� ������: \n";
    for (unsigned int i = 0; i < size; i++)
        cout << minDist[i] << endl;

    // �������������� ����
    int ver[size]; // ������ ���������� ������
    int end = 4; // ������ �������� ������� = 5 - 1
    ver[0] = end + 1; // ��������� ������� - �������� �������
    int k = 1; // ������ ���������� �������
    int weight = minDist[end]; // ��� �������� �������

    while (end != 0) // ���� �� ����� �� ��������� �������
    {
        for (unsigned int i = 0; i < size; i++) // ������������� ��� �������
            if (mat[i][end] != 0) // ���� ����� ����
            {
                int temp = weight - mat[i][end]; // ���������� ��� ���� �� ���������� �������
                if (temp == minDist[i]) // ���� ��� ������ � ������������
                {           // ������ �� ���� ������� � ��� �������
                    weight = temp; // ��������� ����� ���
                    end = i; // ��������� ���������� �������
                    ver[k] = i + 1; // � ���������� �� � ������
                    k++;
                }
            }
    }
    // ����� ���� (��������� ������� ��������� � ����� ������� �� k ���������)
    cout << "\n����� ����������� ����\n";

    for (int i = k - 1; i >= 0; i--)
        cout << ver[i] << endl;
    
    fout.open(FILE_OUT);

    if(!fout)
        return -1;
    
    fout << "���������� ���������� �� ������: "<< endl;

    for (unsigned int i = 0; i < size; i++)
        fout << "[1-" << i + 1 << "]\t" << minDist[i] << endl;

    fout << "���������� ����:" << endl;

    for (int i = k - 1; i >= 0; i--)
        fout << ver[i] << endl;

    fout.close();

    system("pause");
    return 0;
}
